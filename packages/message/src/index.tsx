import React, { isValidElement } from 'react';
import { createRoot, Root } from 'react-dom/client';
import MessageUI, { MessageUIProps } from './MessageUI';
import { MessageItemProps } from './MessageUI/Item';

// duration	默认自动关闭延时，单位秒	number	3
// getContainer	配置渲染节点的输出位置	() => HTMLElement	() => document.body
// maxCount	最大显示数, 超过限制时，最早的消息会被自动关闭	number	-
// prefixCls	消息节点的 className 前缀	string	ant-message	4.5.0
// rtl	是否开启 RTL 模式	boolean	false
// top	消息距离顶部的位置
export interface ConfigProps {
  duration?: number;
  getContainer?: () => HTMLElement | Document;
  maxCount?: number;
  prefixCls?: string;
  rtl?: boolean;
  top?: number;
}

class Message {
  private static instance: Message;
  private messageList: MessageItemProps[] = [];
  private msgRoot: Root | null = null;
  private msgConfig: ConfigProps = {
    duration: 3,
    maxCount: 10,
  };

  static getInstance() {
    if (!this.instance) {
      this.instance = new Message();
    }
    return this.instance;
  }

  render() {
    const { getContainer } = this.msgConfig || {};
    if (!this.msgRoot) {
      const element = document.createElement('div');
      const renderContainer = getContainer?.() || document.body;
      renderContainer.appendChild(element);
      this.msgRoot = createRoot(element);
    }

    this.msgRoot.render(
      <MessageUI defaultConfig={this.msgConfig} list={this.messageList} />,
    );
  }

  config({
    duration,
    getContainer,
    maxCount,
    prefixCls,
    rtl,
    top,
  }: ConfigProps) {
    this.msgConfig = {
      duration,
      getContainer,
      maxCount,
      prefixCls,
      rtl,
      top,
    };
  }

  push(msg: MessageItemProps, callback?: (_msg: MessageItemProps) => void) {
    if (
      this.msgConfig.maxCount &&
      this.messageList.length >= this.msgConfig.maxCount
    ) {
      this.messageList[0].msgRef?.current?.close();
    }
    const pushData = {
      ...msg,
      onClose: () => {
        this.del(msg.key);
        msg?.onClose?.();
        callback?.(pushData);
      },
    };
    this.messageList.push(pushData);
    this.render();
  }

  update(msg: MessageItemProps, callback?: (_msg: MessageItemProps) => void) {
    this.messageList = this.messageList.map((item) => {
      let newItem = { ...item };
      if (item.msgId === msg.key || item.key === msg.key) {
        newItem = {
          ...item,
          ...msg,
          onClose: () => {
            this.del(msg.key);
            msg?.onClose?.();
            callback?.(msg);
          },
        };
      }
      return newItem;
    });
    this.render();
  }

  openAsync(msg: MessageItemProps) {
    const key =
      msg.key || `${Date.now()}${this.messageList.length}${Math.random() * 10}`;
    msg.key = msg.msgId = key;
    if (
      this.messageList.some((item) => item.key === key || item.msgId === key)
    ) {
      this.update(msg);
    } else {
      this.push(msg);
    }
    const find = this.messageList.find((item) => item.msgId === key);
    return () => {
      find?.msgRef?.current?.close?.();
    };
  }

  open(msg: MessageItemProps) {
    return new Promise<MessageItemProps>((resolve) => {
      const key =
        msg.key ||
        `${Date.now()}${this.messageList.length}${Math.random() * 10}`;
      msg.key = msg.msgId = key;
      if (
        this.messageList.some((item) => item.key === key || item.msgId === key)
      ) {
        this.update(msg, resolve);
      } else {
        this.push(msg, resolve);
      }
    });
  }

  del(key?: string) {
    this.messageList = this.messageList.filter((item) => item.msgId !== key);
    this.render();
  }
  success(config: MessageItemProps): Promise<MessageItemProps>;
  success(content: MessageItemProps['content']): Promise<MessageItemProps>;
  success(
    content: MessageItemProps['content'],
    duration?: ConfigProps['duration'],
  ): Promise<MessageItemProps>;
  success(
    content: MessageItemProps['content'] | MessageItemProps,
    duration?: ConfigProps['duration'],
    onClose?: MessageItemProps['onClose'],
  ) {
    if (
      typeof content === 'object' &&
      content !== null &&
      !isValidElement(content)
    ) {
      return this.open({
        ...content,
        type: 'success',
      });
    }
    return this.open({ content, duration, onClose, type: 'success' });
  }
  error(
    content: MessageItemProps['content'],
    duration?: ConfigProps['duration'],
    onClose?: MessageItemProps['onClose'],
  ) {
    return this.open({ content, duration, onClose, type: 'error' });
  }
  info(
    content: MessageItemProps['content'],
    duration?: ConfigProps['duration'],
    onClose?: MessageItemProps['onClose'],
  ) {
    return this.open({
      content,
      duration,
      type: 'info',
      onClose,
    });
  }
  warning(
    content: MessageItemProps['content'],
    duration?: ConfigProps['duration'],
    onClose?: MessageItemProps['onClose'],
  ) {
    return this.open({ content, duration, onClose, type: 'warning' });
  }
  warn(
    content: MessageItemProps['content'],
    duration?: ConfigProps['duration'],
    onClose?: MessageUIProps['onClose'],
  ) {
    return this.warning(content, duration, onClose);
  }

  loading(
    content: MessageItemProps['content'],
    duration?: ConfigProps['duration'],
  ): Promise<MessageItemProps>;
  loading(
    content: MessageItemProps['content'],
    duration?: ConfigProps['duration'],
    onClose?: MessageItemProps['onClose'],
  ): () => void;
  loading(config: MessageItemProps): Promise<MessageItemProps>;
  loading(
    content: MessageItemProps['content'] | MessageItemProps,
    duration?: ConfigProps['duration'],
    onClose?: MessageItemProps['onClose'],
  ) {
    if (
      typeof content === 'object' &&
      content !== null &&
      !isValidElement(content)
    ) {
      return this.open({
        ...content,
        type: 'loading',
      });
    }
    if (typeof onClose === 'function') {
      return this.openAsync({ content, duration, onClose, type: 'loading' });
    }
    return this.open({ content, duration, onClose, type: 'loading' });
  }

  // useMessage() {

  // }

  destroy() {
    this.messageList.forEach((item) => {
      item.msgRef?.current?.close?.();
    });
    this.messageList = [];
    this.render();
  }
}

const message = Message.getInstance();

export default message;
