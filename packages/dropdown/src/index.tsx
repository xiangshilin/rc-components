import Button, { DropdownButtonProps } from './Button';
import Dropdown, { DropdownProps } from './Dropdown';

const DropdownExport = Dropdown as React.FC<DropdownProps> & {
  Button: React.FC<DropdownButtonProps>;
};

DropdownExport.Button = Button;

export default DropdownExport;
