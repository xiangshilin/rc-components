import { Segmented } from 'mumu-design';
import React from 'react';

export default () => (
  <Segmented options={['Daily', 'Weekly', 'Monthly', 'Quarterly', 'Yearly']} />
);
