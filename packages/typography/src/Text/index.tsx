import React, { FC, MouseEventHandler, PropsWithChildren } from 'react';
import Base from '../Base';

export interface TextProps extends PropsWithChildren {
  onClick?: MouseEventHandler<HTMLAnchorElement>;
  type?: 'secondary' | 'success' | 'warning' | 'danger';
  code?: boolean;
  disabled?: boolean;
  mark?: boolean;
  keyboard?: boolean;
  underline?: boolean;
  delete?: boolean;
  strong?: boolean;
  italic?: boolean;
}

const Text: FC<TextProps> = ({
  children,
  onClick,
  mark,
  code,
  keyboard,
  italic,
  type,
  disabled,
}) => {
  return (
    <Base
      style={{ fontSize: 14 }}
      {...{
        onClick,
        mark,
        code,
        keyboard,
        italic,
        type,
        disabled,
      }}
    >
      {children}
    </Base>
  );
};

export default Text;
