---
demo:
  cols: 1
toc: content
group:
  title: 导航
  order: 1
---

# Dropdown 下拉菜单

向下弹出的列表。

## 何时使用

当页面上的操作命令过多时，用此组件可以收纳操作元素。点击或移入触点，会出现一个下拉菜单。可在列表中进行选择，并执行相应的命令。

用于收罗一组命令操作。
Select 用于选择，而 Dropdown 是命令集合。

## 代码演示

<code 
  src="./demo/01.tsx" 
  description="最简单的下拉菜单。">
基本
</code>

<code 
  src="./demo/02.tsx" 
  description="支持 6 个弹出位置。">
弹出位置
</code>

<code 
  src="./demo/03.tsx" 
  description="可以展示一个小箭头。">
箭头
</code>

<code 
  src="./demo/04.tsx" 
  description="设置 `arrow` 为 `{ pointAtCenter: true }` 后，箭头将指向目标元素的中心。">
箭头指向
</code>

<code 
  src="./demo/05.tsx" 
  description="分割线和不可用菜单项。">
其他元素
</code>

<code 
  src="./demo/06.tsx" 
  description="默认是移入触发菜单，可以点击触发。">
触发方式
</code>
