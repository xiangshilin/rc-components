'use strict';

const dropdown = require('..');
const assert = require('assert').strict;

assert.strictEqual(dropdown(), 'Hello from dropdown');
console.info('dropdown tests passed');
