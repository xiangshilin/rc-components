import React, { FC, MouseEventHandler, PropsWithChildren } from 'react';
import Base from '../Base';

export interface LinkProps extends PropsWithChildren {
  onClick?: MouseEventHandler<HTMLAnchorElement>;
  href?: string;
  target?: string;
}

const Link: FC<LinkProps> = ({ children, onClick, target, href }) => {
  return (
    <Base>
      <a
        className="m-typography-link"
        target={target}
        href={href}
        onClick={onClick}
      >
        {children}
      </a>
    </Base>
  );
};

export default Link;
