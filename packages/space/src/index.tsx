import Compact, { CompactProps } from './Compact';
import Space, { SpaceProps } from './Space';

const SpaceExport = Space as React.FC<SpaceProps> & {
  Compact: React.FC<CompactProps>;
};

SpaceExport.Compact = Compact;

export default SpaceExport;
