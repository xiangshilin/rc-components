import Option from './Option';
import Select, { SelectProps } from './Select';

const SelectExport = Select as React.FC<SelectProps> & {
  Option: typeof Option;
};

SelectExport.Option = Option;

export default SelectExport;
