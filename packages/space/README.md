---
demo:
  cols: 2
toc: content
group: 布局
order: 3
---

# Space 间距

设置组件之间的间距。

## 何时使用

避免组件紧贴在一起，拉开统一的空间。

- 适合行内元素的水平间距。
- 可以设置各种水平对齐方式。
- 需要表单组件之间紧凑连接且合并边框时，使用 Space.Compact。

## 代码演示

<code 
  src="./demo/01.tsx" 
  description="相邻组件水平间距。">
基本用法
</code>

<code 
  src="./demo/02.tsx" 
  description="相邻组件垂直间距。">
垂直间距
</code>

<code 
  src="./demo/03.tsx" 
  description="间距预设大、中、小三种大小。<br />
  通过设置 size 为 large middle 分别把间距设为大、中间距。若不设置 size，则间距为小。">
间距大小
</code>

<code 
  src="./demo/04.tsx" 
  description="设置对齐模式。">
对齐
</code>

<code
  src="./demo/05.tsx"
  description="自定义间距大小。">
自定义尺寸
</code>

<code
  src="./demo/06.tsx"
  description="自动换行。">
自动换行
</code>

<code
  src="./demo/07.tsx"
  description="相邻组件分隔符。">
分隔符
</code>
