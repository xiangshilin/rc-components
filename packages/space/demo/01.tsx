import { Button, Space } from 'mumu-design';
import React from 'react';

const App: React.FC = () => (
  <Space>
    Space
    <Button type="primary">Button</Button>
    <Button>Confirm1</Button>
    <Button>Confirm2</Button>
  </Space>
);

export default App;
