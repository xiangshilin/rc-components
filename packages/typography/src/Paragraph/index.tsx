import React, { FC, MouseEventHandler, PropsWithChildren } from 'react';

export interface ParagraphProps extends PropsWithChildren {
  onClick?: MouseEventHandler<HTMLAnchorElement>;
}

const Paragraph: FC<ParagraphProps> = ({ children, onClick }) => {
  return <span onClick={onClick}>{children}</span>;
};

export default Paragraph;
