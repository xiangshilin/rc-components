import Space from '@mumu-design/space';
import React, {
  Children,
  cloneElement,
  type CSSProperties,
  type FC,
  type PropsWithChildren,
} from 'react';
import './index.less';

export interface GroupProps extends PropsWithChildren {
  onChange?: (val: string | number | readonly string[] | undefined) => void;
  name?: string;
  value?: string | number | readonly string[] | undefined;
  defaultValue?: string | number | readonly string[] | undefined;
  style?: CSSProperties;
}

const Group: FC<GroupProps> = ({
  children,
  onChange,
  name,
  value,
  defaultValue,
  style,
}) => {
  const _children = Children.toArray(children);

  return (
    <Space.Compact style={style} className="m-radio-group">
      {_children.map((Node: any, index: number) => {
        return cloneElement(Node, {
          ...Node.props,
          onChange,
          name,
          defaultValue: value || defaultValue,
          groupIdx: index,
          groupLength: _children.length,
        });
      })}
    </Space.Compact>
  );
};

export default Group;
