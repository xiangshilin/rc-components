import React, { FC, PropsWithChildren } from 'react';
import Base from '../Base';

export type TypographyProps = PropsWithChildren;

const Typography: FC<TypographyProps> = ({ children }) => {
  return (
    <article>
      <Base>{children}</Base>
    </article>
  );
};

export default Typography;
