import { Button, Radio, Space } from 'mumu-design';
import type { SpaceSize } from 'mumu-design/space/lib/Space';
import React, { useState } from 'react';

const App: React.FC = () => {
  const [size, setSize] = useState<SpaceSize | [SpaceSize, SpaceSize]>('small');

  return (
    <>
      <Radio
        defaultChecked
        name="size"
        value="small"
        onChange={(e) => setSize(e.target.value as any)}
      >
        Small
      </Radio>
      <Radio
        name="size"
        value="middle"
        onChange={(e) => setSize(e.target.value as any)}
      >
        Middle
      </Radio>
      <Radio
        name="size"
        value="large"
        onChange={(e) => setSize(e.target.value as any)}
      >
        Large
      </Radio>
      <br />
      <br />
      <Space size={size}>
        <Button type="primary">Primary</Button>
        <Button>Default</Button>
        <Button type="dashed">Dashed</Button>
        <Button type="link">Link</Button>
      </Space>
    </>
  );
};

export default App;
