---
demo:
  cols: 1
toc: content
group: 通用
order: 3
---

# Typography 排版

文本的基本格式。

## 何时使用

当需要展示标题、段落、列表内容时使用，如文章/博客/日志的文本样式。
当需要一列基于文本的基础操作时，如拷贝/省略/可编辑。

## 代码演示

<code 
  src="./demo/01.tsx" 
  description="展示文档样例。">
基本
</code>
<code 
  src="./demo/02.tsx" 
  description="展示不同级别的标题。">
标题组件
</code>
<code 
  src="./demo/03.tsx" 
  description="内置不同样式的文本以及超链接组件。">
文本与超链接组件
</code>
