import { Down } from '@mumu-design/icons';
import type { MenuProps } from '@mumu-design/menu/src/Menu';
import { Dropdown, Space } from 'mumu-design';
import React from 'react';

const items: MenuProps['items'] = [
  {
    label: <a href="https://www.antgroup.com">1st menu item</a>,
    key: '0',
  },
  {
    label: <a href="https://www.aliyun.com">2nd menu item</a>,
    key: '1',
  },
  {
    type: 'divider',
  },
  {
    label: '3rd menu item',
    key: '3',
  },
];

const App: React.FC = () => (
  <Dropdown menu={{ items }} trigger={['click']}>
    <a onClick={(e) => e.preventDefault()}>
      <Space>
        Click me
        <Down />
      </Space>
    </a>
  </Dropdown>
);

export default App;
