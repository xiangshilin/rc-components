import { classnames } from '@mumu-design/utils';
import React, { ReactNode, type FC } from 'react';
import { renderItem } from '../Menu';
import './index.less';

type MenuItemType = {
  children?: ItemType[];
  disabled?: boolean;
  icon?: ReactNode;
  key: string;
  label: ReactNode;
  popupClassName?: string;
  popupOffset?: [number, number];
  onTitleClick?: ({ key, domEvent, item }: any) => void;
  theme?: 'light' | 'dark';
  danger?: boolean;
  id?: string;
  title: string;
};
type MenuItemGroupType = {
  children?: ItemType[];
  label: ReactNode;
};
type MenuDividerType = {
  dashed: boolean;
};
type MenuLineType = { type: 'divider' | 'group' };

export type ItemType =
  | MenuItemType
  | MenuItemGroupType
  | MenuDividerType
  | MenuLineType;

export interface MenuItemProps
  extends MenuItemType,
    MenuItemGroupType,
    MenuDividerType,
    MenuLineType {}

const MenuItem: FC<MenuItemProps> = (props) => {
  const renderContent = () => {
    if (props.label) return props.label;
    if (props.title) return props.title;
    if (props.children)
      return renderItem(props.children, undefined, props.onTitleClick);
  };

  return (
    <div
      className={classnames('m-menu-item')}
      title={props.title}
      onClick={(e) => {
        props?.onTitleClick?.({ key: props.id, domEvent: e, item: props });
      }}
    >
      {renderContent()}
    </div>
  );
};

export default MenuItem;
