import React, {
  forwardRef,
  ForwardRefRenderFunction,
  useImperativeHandle,
  useState,
} from 'react';
import { ButtonProps } from '.';

type WaterWaveProps = ButtonProps;

const WaterWaveItem = ({ animationName }: { animationName: string }) => {
  const [show, setShow] = useState(true);

  if (!show) return null;

  return (
    <div
      onAnimationEnd={() => {
        setShow(false);
      }}
      style={{ animationName }}
      className="m-btn-water-wave"
    ></div>
  );
};

const WaterWave: ForwardRefRenderFunction<any, WaterWaveProps> = (
  { shape, type, groupIdx, groupLength },
  ref,
) => {
  const [waterWaveList, setWaterWaveList] = useState<string[]>([]);

  const pushWaterWaveList = () => {
    const _waterWaveList = [...waterWaveList];
    _waterWaveList.push('waterWave');
    setWaterWaveList(_waterWaveList);
  };

  const getAnimationName = () => {
    if (['link', 'text'].includes(type as any)) return 'unset';
    if (shape === 'circle') return 'water-wave-circle';
    if (typeof groupIdx === 'number') {
      if (groupIdx === 0) return 'water-wave-first';
      if (typeof groupLength === 'number' && groupIdx === groupLength - 1)
        return 'water-wave-last';
      return 'water-wave-center';
    }
    return 'water-wave';
  };

  useImperativeHandle(
    ref,
    () => ({
      add() {
        pushWaterWaveList();
      },
    }),
    [waterWaveList],
  );

  return (
    <>
      {waterWaveList?.map((_, index) => {
        return <WaterWaveItem key={index} animationName={getAnimationName()} />;
      })}
    </>
  );
};

export default forwardRef(WaterWave);
