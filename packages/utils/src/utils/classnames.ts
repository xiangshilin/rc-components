export default function classnames(...args: any[]) {
  let className = '';
  args.forEach((item) => {
    if (typeof item === 'string') {
      className += className === '' ? `${item}` : ` ${item}`;
    } else if (item !== null && typeof item === 'object') {
      Object.keys(item).forEach((key) => {
        if (!!item[key]) {
          className += className === '' ? `${key}` : ` ${key}`;
        }
      });
    }
  });
  return className;
}
