import { classnames } from '@mumu-design/utils';
import React, { FC, ReactNode, useEffect, useRef, useState } from 'react';
import './index.less';

export interface SegmentedOption {
  label?: ReactNode;
  value: string;
  icon?: ReactNode;
  disabled?: boolean;
  className?: string;
}

interface SegmentedProps {
  /**
   * @description 将宽度调整为父元素宽度的选项
   * @default false
   */
  block?: boolean;
  /**
   * @description 默认选中的值
   */
  defaultValue?: string | number;
  /**
   * @description 是否禁用
   * @default false
   */
  disabled?: boolean;
  /**
   * @description 选项变化时的回调函数
   */
  onChange?: (val: string | number) => void;
  /**
   * @description 数据化配置选项内容
   * @default []
   */
  options: (string | number | SegmentedOption)[];
  /**
   * @description 控件尺寸
   * @default 'middle'
   */
  size?: 'large' | 'middle' | 'small';
  /**
   * @description 当前选中的值
   */
  value?: string | number;
}

const getDefaultVal = (options: (string | number | SegmentedOption)[]) => {
  const firstVal = options?.[0];
  if (typeof firstVal === 'number' || typeof firstVal === 'string') {
    return firstVal;
  }
  return firstVal?.value;
};

const Segmented: FC<SegmentedProps> = ({
  options,
  value,
  defaultValue = getDefaultVal(options),
  onChange,
  block,
  size,
}) => {
  const [selfValue, setSelfValue] = useState<string | number>();
  const [domSize, setDomSize] = useState<any>();
  const nodeRef = useRef<HTMLDivElement>(null);

  const getOptionVal = (option: number | string | SegmentedOption) => {
    if (typeof option === 'string' || typeof option === 'number') {
      return option;
    }
    return option?.value;
  };

  const renderItem = (
    option: number | string | SegmentedOption,
    index: number,
  ) => {
    const isSelected = () => {
      return selfValue === getOptionVal(option);
    };

    const renderLabel = () => {
      if (typeof option === 'string' || typeof option === 'number') {
        return option;
      }
      return (
        <div className="m-segmented-item-label">
          <span>{option?.icon}</span>
          {option?.label && (
            <span style={{ marginLeft: !!option.icon ? 6 : 0 }}>
              {option.label}
            </span>
          )}
        </div>
      );
    };

    return (
      <div
        key={getOptionVal(option) || index}
        onClick={() => {
          if (typeof option === 'object' && option?.disabled) {
            return;
          }
          setSelfValue(getOptionVal(option));
          onChange?.(getOptionVal(option));
        }}
        className={classnames('m-segmented-item', {
          selected: isSelected(),
          disabled: typeof option === 'object' ? option?.disabled : false,
        })}
      >
        {renderLabel()}
      </div>
    );
  };

  const render = () => {
    if (Array.isArray(options)) {
      return options.map((option, index) => {
        return renderItem(option, index);
      });
    }
    return <></>;
  };

  const getSize = (target: HTMLDivElement | Element | undefined) => {
    const { offsetHeight, offsetWidth, offsetLeft, offsetTop } = target as any;
    return {
      left: offsetLeft,
      top: offsetTop,
      width: offsetWidth,
      height: offsetHeight,
    };
  };

  const computedSize = () => {
    let selectedIndex = 0;
    options?.forEach((option, index) => {
      if (selfValue === getOptionVal(option)) {
        selectedIndex = index;
        return;
      }
    });
    const size = getSize(nodeRef.current?.children?.[selectedIndex]);
    setDomSize(size);
  };

  useEffect(() => {
    setSelfValue(value);
  }, [value]);

  useEffect(() => {
    computedSize();
  }, [selfValue]);

  useEffect(() => {
    setSelfValue(defaultValue);
    computedSize();
  }, []);

  return (
    <div
      ref={nodeRef}
      className={classnames('m-segmented', {
        block: block,
        lg: size === 'large',
        sm: size === 'small',
      })}
    >
      {render()}
      <div
        style={{
          left: domSize?.left,
          top: domSize?.top,
          width: domSize?.width,
          height: domSize?.height,
        }}
        className="m-segmented-slider"
      ></div>
    </div>
  );
};

export default Segmented;
