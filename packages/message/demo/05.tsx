import { Button, message } from 'mumu-design';
import React from 'react';

const success = () => {
  message
    .loading('Action in progress..', 2.5)
    .then(() => message.success('Loading finished', 2.5))
    .then(() => message.info('Loading finished is finished', 2.5));
};

const App: React.FC = () => (
  <Button onClick={success}>Display sequential messages</Button>
);

export default App;
