import { defineConfig } from 'dumi';

export default defineConfig({
  outputPath: 'docs-dist',
  base: '/rc-components/',
  publicPath: '/rc-components/',
  themeConfig: {
    name: 'Mumu',
    nav: [
      { title: '指南', link: '/guide' },
      { title: '组件', link: '/components' },
    ],
  },
  hash: true,
  // apiParser: {},
  resolve: {
    atomDirs: [{ type: 'component', dir: 'packages' }],
    entryFile: './packages/index.ts',
  },
  styles: [
    '.dumi-default-sidebar { width: 230px!important }',
    '.dumi-default-header-content, .dumi-default-doc-layout > main { max-width: 99%!important;}',
  ],
});
