import { classnames } from '@mumu-design/utils';
import React, { CSSProperties, FunctionComponent, ReactElement } from 'react';
import './icon.less';

// className	设置图标的样式名	string	-
// rotate	图标旋转角度（IE9 无效）	number	-
// spin	是否有旋转动画	boolean	false
// style	设置图标的样式，例如 fontSize 和 color	CSSProperties	-
// twoToneColor	仅适用双色图标。设置双色图标的主要颜色	string (十六进制颜色)	-

// component	控制如何渲染图标，通常是一个渲染根标签为 <svg> 的 React 组件	ComponentType<CustomIconComponentProps>	-
// rotate	图标旋转角度（IE9 无效）	number	-
// spin	是否有旋转动画	boolean	false
// style	设置图标的样式，例如 fontSize 和 color	CSSProperties	-
interface IconProps {
  spin?: boolean;
  className?: string;
  rotate?: number;
  style?: CSSProperties;
  twoToneColor?: string;
  component?: ReactElement;
}

const Icon: FunctionComponent<IconProps> = ({
  spin,
  style,
  className,
  component,
}) => {
  return (
    <span
      style={style}
      className={classnames(`m-icons`, className, {
        spin: spin,
      })}
    >
      {component}
    </span>
  );
};

export default Icon;
