# Mumu Design of React

[![NPM version](https://img.shields.io/npm/v/mumu-design.svg?style=flat)](https://npmjs.org/package/mumu-design)
[![NPM downloads](http://img.shields.io/npm/dm/mumu-design.svg?style=flat)](https://npmjs.org/package/mumu-design)

高仿 antd 组件。

## ✨ 特性

🌈 提炼自企业级中后台产品的交互语言和视觉风格。

📦 开箱即用的高质量 `React` 组件。

🛡 使用 `TypeScript` 开发，提供完整的类型定义文件。

⚙️ 全链路开发和设计工具体系。

🌍 数十个国际化语言支持。

🎨 深入每个细节的主题定制能力。

## 兼容环境

- 现代浏览器
- 支持服务端渲染。
- Electron

## 版本

稳定版：[![NPM version](https://img.shields.io/npm/v/mumu-design.svg?style=flat)](https://npmjs.org/package/mumu-design)

## 安装

使用 npm 或 yarn 安装
我们推荐使用 npm 或 yarn 的方式进行开发，不仅可在开发环境轻松调试，也可放心地在生产环境打包部署使用，享受整个生态圈和工具链带来的诸多好处。

```
$ npm install mumu-design --save
$ yarn add mumu-design
```

如果你的网络环境不佳，推荐使用 cnpm。

## 浏览器引入

在浏览器中使用 script 和 link 标签直接引入文件，并使用全局变量 `MumuDesign`。

我们在 npm 发布包内的 dist 目录下提供了 `mumu-design.js`、`mumu-design.min.js` 和 `reset.css`。你也可以通过 `CDNJS`， 或 `UNPKG` 进行下载。

强烈不推荐使用已构建文件，这样无法按需加载，而且难以获得底层依赖模块的 bug 快速修复支持。

注意：`mumu-design.js` 和 `mumu-design.min.js` 依赖 `react`、`react-dom`、`dayjs`，请确保提前引入这些文件。

## 示例

```
import React from 'react';
import { DatePicker } from 'mumu-design';

const App = () => {
  return <DatePicker />;
};

export default App;
```

## 按需加载

`mumu-design` 默认支持基于 `ES modules` 的 tree shaking。

## TypeScript

`mumu-design` 使用 `TypeScript` 进行书写并提供了完整的定义文件。（不要引用 `@types/mumu-design`）。
