import { classnames } from '@mumu-design/utils';
import React, { ReactNode, type FC } from 'react';
import './index.less';

type OptionItemType = {
  children?: ItemType[];
  disabled?: boolean;
  icon?: ReactNode;
  key: string;
  label: ReactNode;
  popupClassName?: string;
  popupOffset?: [number, number];
  onTitleClick?: ({ key, domEvent, item }: any) => void;
  theme?: 'light' | 'dark';
  danger?: boolean;
  id?: string;
  title: string;
};
type OptionItemGroupType = {
  children?: ItemType[];
  label: ReactNode;
};
type OptionDividerType = {
  dashed: boolean;
};
type OptionLineType = { type: 'divider' | 'group' };

export type ItemType =
  | OptionItemType
  | OptionItemGroupType
  | OptionDividerType
  | OptionLineType;

export interface OptionItemProps
  extends OptionItemType,
    OptionItemGroupType,
    OptionDividerType,
    OptionLineType {}

const Option: FC<OptionItemProps> = (props) => {
  const renderContent = () => {
    if (props.label) return props.label;
    if (props.title) return props.title;
  };

  if (props.type === 'divider') {
    return <div className="m-dropdown-menu-item-divider"></div>;
  }

  return (
    <div
      className={classnames('m-dropdown-menu-item', {
        disable: props.disabled,
        danger: props.danger,
      })}
      title={props.title}
      onClick={(e) => {
        console.log(111);

        props?.onTitleClick?.({ key: props.id, domEvent: e, item: props });
      }}
    >
      {renderContent()}
    </div>
  );
};

export default Option;
