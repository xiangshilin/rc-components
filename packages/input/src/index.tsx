import Link, { LinkProps } from './Link';
import Text, { TextProps } from './Text';

const RadioExport = {} as {
  Link: React.FC<LinkProps>;
  Text: React.FC<TextProps>;
};

RadioExport.Link = Link;
RadioExport.Text = Text;

export default RadioExport;
