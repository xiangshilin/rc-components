---
demo:
  cols: 1
toc: content
group: 通用
order: 2
---

# Icon 图标

语义化的矢量图形。使用图标组件，你需要安装 @mumu-design/icons 图标组件包：

```
npm install --save @mumu-design/icons
```

## 图标列表

<code src="./demo/01.tsx" ></code>
