import { classnames, useSize } from '@mumu-design/utils';
import React, {
  Children,
  cloneElement,
  isValidElement,
  PropsWithChildren,
  ReactNode,
  useCallback,
  useEffect,
  useRef,
  useState,
  type FC,
} from 'react';
import { createPortal } from 'react-dom';
import './index.less';
import {
  computedLeft,
  computedTop,
  isBottom,
  isLeft,
  isRight,
  isTop,
  isTrigger,
} from './utils';

export type Placement =
  | 'top'
  | 'left'
  | 'right'
  | 'bottom'
  | 'topLeft'
  | 'topRight'
  | 'bottomLeft'
  | 'bottomRight'
  | 'leftTop'
  | 'leftBottom'
  | 'rightTop'
  | 'rightBottom';
export type Trigger = 'hover' | 'focus' | 'click' | 'contextMenu';

export interface TooltipProps extends PropsWithChildren {
  arrow?: boolean | { pointAtCenter: boolean };
  autoAdjustOverflow?: boolean;
  color?: string;
  defaultOpen?: boolean;
  destroyTooltipOnHide?: boolean | { keepParent?: boolean };
  getPopupContainer?: (triggerNode?: HTMLElement | null) => HTMLElement;
  mouseEnterDelay?: number;
  mouseLeaveDelay?: number;
  overlayClassName?: string;
  overlayStyle?: object;
  overlayInnerStyle?: object;
  placement?: Placement;
  trigger?: Trigger | Trigger[];
  open?: boolean;
  zIndex?: number;
  onOpenChange?: (open: boolean) => void;
  title?: string | ReactNode | (() => ReactNode);
  className?: string;
}

const Tooltip: FC<TooltipProps> = ({
  children,
  title,
  getPopupContainer,
  className,
  trigger = 'hover',
  placement = 'top',
  destroyTooltipOnHide,
  arrow = true,
  color,
  overlayInnerStyle,
  open,
}) => {
  const triggerNode = useRef<HTMLDivElement & HTMLElement>(null);
  const tooltipNode = useRef<HTMLDivElement & HTMLElement>(null);
  const isEnterTootip = useRef(false);
  const triggerNodeSize = useSize(triggerNode);
  const tooltipNodeSize = useSize(tooltipNode);
  const [selfOpen, setSelfOpen] = useState(false);
  const [firstOpen, setFirstOpen] = useState(false);
  const [uiHide, setUIHide] = useState(false);

  const onClose = () => {
    setUIHide(true);
  };

  const onOpen = () => {
    if (!selfOpen) {
      setFirstOpen(true);
    }
    setUIHide(false);
    setSelfOpen(true);
  };

  const onTriggerOpen = () => {
    if (typeof open === 'boolean') return;
    onOpen();
  };

  const onTriggerClose = () => {
    if (typeof open === 'boolean') return;
    onClose();
  };

  const onParentOpen = () => {
    onOpen();
  };

  const onParentClose = () => {
    onClose();
  };

  const onBodyClick = useCallback(() => {
    if (isTrigger('click', trigger)) {
      onClose();
    }
  }, [trigger]);

  const renderTitle = () => {
    if (isValidElement(title)) return cloneElement(title);
    if (typeof title === 'function') {
      return title();
    }
    return title;
  };

  const renderTooltip = () => {
    const offsetContent = arrow ? 13 : 3;

    const getContentDom = () => {
      return (
        <div
          style={{
            ...overlayInnerStyle,
            backgroundColor: color,
          }}
          className={classnames('m-tooltip-content')}
        >
          {renderTitle()}
        </div>
      );
    };

    const getArrowDom = () => {
      if (!arrow) return <></>;
      return (
        <div className={classnames('m-tooltip-arrow')}>
          <div
            style={{ backgroundColor: color }}
            className="m-tooltip-arrow-content"
          ></div>
        </div>
      );
    };

    const renderContent = () => {
      if (typeof destroyTooltipOnHide !== 'boolean') {
        if (destroyTooltipOnHide?.keepParent) {
          if (!selfOpen || !title) return <></>;
        }
      }

      if (!firstOpen) {
        return <></>;
      }

      const getDisplay = () => {
        if (isLeft(placement) || isRight(placement)) {
          if (!selfOpen || !title) {
            return 'none';
          }
          return 'flex';
        }
        if (!selfOpen || !title) {
          return 'none';
        }
        return 'block';
      };
      if (isTop(placement) || isLeft(placement))
        return (
          <div style={{ display: getDisplay() }}>
            {getContentDom()}
            <div
              style={{
                width: isLeft(placement) ? offsetContent : 'auto',
                height: isTop(placement) ? offsetContent : 'auto',
              }}
            >
              {getArrowDom()}
            </div>
          </div>
        );
      if (isBottom(placement) || isRight(placement))
        return (
          <div style={{ display: getDisplay() }}>
            <div
              style={{
                width: isRight(placement) ? offsetContent : 'auto',
                height: isBottom(placement) ? offsetContent : 'auto',
              }}
            >
              {getArrowDom()}
            </div>
            {getContentDom()}
          </div>
        );
    };

    return createPortal(
      <div
        ref={tooltipNode}
        style={{
          top: computedTop(triggerNodeSize, tooltipNodeSize, placement),
          left: computedLeft(triggerNodeSize, tooltipNodeSize, placement),
        }}
        onMouseEnter={() => {
          isEnterTootip.current = true;
        }}
        onMouseLeave={() => {
          isEnterTootip.current = false;
          if (isTrigger('hover', trigger)) {
            onTriggerClose();
          }
        }}
        className={classnames(
          'm-tooltip',
          {
            ['hide']: uiHide,
            ['top']: placement === 'top',
            ['top-left']: placement === 'topLeft',
            ['top-right']: placement === 'topRight',
            ['bottom']: placement === 'bottom',
            ['bottom-left']: placement === 'bottomLeft',
            ['bottom-right']: placement === 'bottomRight',
            ['left']: placement === 'left',
            ['left-top']: placement === 'leftTop',
            ['left-bottom']: placement === 'leftBottom',
            ['right']: placement === 'right',
            ['right-top']: placement === 'rightTop',
            ['right-bottom']: placement === 'rightBottom',
          },
          className,
        )}
      >
        {renderContent()}
      </div>,
      getPopupContainer?.(triggerNode.current) || document.body,
    );
  };

  const animationEnd = useCallback(() => {
    setSelfOpen(false);
  }, [uiHide]);

  useEffect(() => {
    document.body.addEventListener('click', onBodyClick);
    return () => {
      document.body.removeEventListener('click', onBodyClick);
    };
  }, []);

  useEffect(() => {
    if (uiHide) {
      tooltipNode.current?.addEventListener('animationend', animationEnd);
    }
    return () => {
      tooltipNode.current?.removeEventListener('animationend', animationEnd);
    };
  }, [uiHide]);

  useEffect(() => {
    if (typeof open === 'boolean') {
      if (open) {
        onParentOpen();
      } else {
        onParentClose();
      }
    }
  }, [open]);

  return (
    <>
      {Children.toArray(children).map((Node: any) => {
        return cloneElement(Node, {
          ...Node.props,
          ref: triggerNode,
          onMouseEnter: () => {
            if (isTrigger('hover', trigger)) {
              onTriggerOpen();
            }
          },
          onMouseLeave: () => {
            if (isTrigger('hover', trigger)) {
              setTimeout(() => {
                if (!isEnterTootip.current) {
                  onTriggerClose();
                }
              }, 100);
            }
          },
          onClick: (e: any) => {
            e.stopPropagation();
            if (isTrigger('click', trigger)) {
              onTriggerOpen();
            }
          },
          onContextMenu: () => {
            if (isTrigger('contextMenu', trigger)) {
              onTriggerOpen();
            }
          },
          className: classnames(
            {
              ['m-tooltip-open']: selfOpen,
            },
            Node.props?.className,
          ),
        });
      })}
      {renderTooltip()}
    </>
  );
};

export default Tooltip;
