import { classnames } from '@mumu-design/utils';
import React, {
  CSSProperties,
  FC,
  MouseEventHandler,
  PropsWithChildren,
} from 'react';
import './index.less';

export interface BaseProps extends PropsWithChildren {
  onClick?: MouseEventHandler<HTMLAnchorElement>;
  type?: 'secondary' | 'success' | 'warning' | 'danger';
  code?: boolean;
  disabled?: boolean;
  mark?: boolean;
  keyboard?: boolean;
  underline?: boolean;
  delete?: boolean;
  strong?: boolean;
  italic?: boolean;
  className?: string;
  style?: CSSProperties;
}

const Base: FC<BaseProps> = ({
  children,
  onClick,
  mark,
  code,
  keyboard,
  italic,
  type,
  disabled,
  className,
  style,
}) => {
  const render = () => {
    if (mark) {
      return <mark>{children}</mark>;
    }
    if (code) {
      return <code>{children}</code>;
    }
    if (keyboard) {
      return <kbd>{children}</kbd>;
    }
    if (italic) {
      return <i>{children}</i>;
    }
    return children;
  };
  return (
    <span
      style={style}
      className={classnames(className, 'm-typography', {
        [`m-typography-${type}`]: !!type,
        ['m-typography-disabled']: disabled,
      })}
      onClick={onClick}
    >
      {render()}
    </span>
  );
};

export default Base;
