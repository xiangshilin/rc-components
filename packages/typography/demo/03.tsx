import { Space, Typography } from 'mumu-design';
import React from 'react';

const { Text, Link } = Typography;

const App: React.FC = () => (
  <Space direction="vertical">
    <Text>Mumu Design (default)</Text>
    <Text type="secondary">Mumu Design (secondary)</Text>
    <Text type="success">Mumu Design (success)</Text>
    <Text type="warning">Mumu Design (warning)</Text>
    <Text type="danger">Mumu Design (danger)</Text>
    <Text disabled>Mumu Design (disabled)</Text>
    <Text mark>Mumu Design (mark)</Text>
    <Text code>Mumu Design (code)</Text>
    <Text keyboard>Mumu Design (keyboard)</Text>
    <Text underline>Mumu Design (underline)</Text>
    <Text delete>Mumu Design (delete)</Text>
    <Text strong>Mumu Design (strong)</Text>
    <Text italic>Mumu Design (italic)</Text>
    <Link href="https://ant.design" target="_blank">
      Mumu Design (Link)
    </Link>
  </Space>
);

export default App;
