import { classnames } from '@mumu-design/utils';
import React, {
  CSSProperties,
  PropsWithChildren,
  ReactNode,
  type FC,
} from 'react';
import './index.less';
import Option, { ItemType, OptionItemProps } from './Item';

export interface SelectProps extends PropsWithChildren {
  defaultOpenKeys?: string[];
  defaultSelectedKeys?: string[];
  expandIcon?:
    | ReactNode
    | ((props: OptionItemProps & { isSubMenu: boolean }) => ReactNode);
  forceSubMenuRender?: boolean;
  inlineCollapsed?: boolean;
  inlineIndent?: number;
  options?: ItemType[];
  mode?: 'vertical' | 'horizontal' | 'inline';
  multiple?: boolean;
  openKeys?: string[];
  overflowedIndicator?: ReactNode;
  selectable?: boolean;
  selectedKeys?: string[];
  style?: CSSProperties;
  subMenuCloseDelay?: number;
  subMenuOpenDelay?: number;
  theme?: 'light' | 'dark';
  triggerSubMenuAction?: 'hover' | 'click';
  onClick?: ({ item, key, keyPath, domEvent }: any) => void;
  onDeselect?: ({ item, key, keyPath, selectedKeys, domEvent }: any) => void;
  onOpenChange?: (openKeys: string[]) => void;
  onSelect?: ({ item, key, keyPath, selectedKeys, domEvent }: any) => void;
}

export const renderItem = (
  items: ItemType[] | undefined,
  onClick?: ({ item, key, keyPath, domEvent }: any) => void,
) => {
  return items?.map((item: any, index: number) => {
    return (
      <Option
        onTitleClick={({ item, key, domEvent }) => {
          onClick?.({ item, key, keyPath: '', selectedKeys: [], domEvent });
        }}
        key={item.key || index}
        id={item.key || index}
        {...item}
      />
    );
  });
};

const DropDownSelect: FC<SelectProps> = ({ options, onClick }) => {
  return (
    <div className={classnames('m-dropdown-menu')}>
      {renderItem(options, onClick)}
    </div>
  );
};

export default DropDownSelect;
