import Menu, { MenuProps } from './Menu';

const MenuExport = Menu as React.FC<MenuProps>;

export default MenuExport;
