import { Size } from '@mumu-design/utils/src/hooks/useSize';
import { Placement, Trigger } from '.';

export const isTrigger = (type: Trigger, trigger?: Trigger | Trigger[]) => {
  if (trigger === type) {
    return true;
  }
  if (Array.isArray(trigger)) {
    if (trigger.includes(type)) {
      return true;
    }
  }
  return false;
};

export const isTop = (placement: Placement) => {
  return ['top', 'topLeft', 'topRight'].includes(placement);
};
export const isBottom = (placement: Placement) => {
  return ['bottom', 'bottomLeft', 'bottomRight'].includes(placement);
};
export const isLeft = (placement: Placement) => {
  return ['left', 'leftTop', 'leftBottom'].includes(placement);
};
export const isRight = (placement: Placement) => {
  return ['right', 'rightTop', 'rightBottom'].includes(placement);
};

export const computedLeft = (
  triggerNodeSize: Size | undefined,
  tooltipNodeSize: Size | undefined,
  placement: Placement,
) => {
  if (typeof triggerNodeSize?.left !== 'number') return;
  if (['top', 'bottom'].includes(placement)) {
    if (typeof tooltipNodeSize?.width !== 'number') return;
    return (
      triggerNodeSize?.left +
      triggerNodeSize?.width / 2 -
      tooltipNodeSize?.width / 2
    );
  }
  if (['topLeft', 'bottomLeft'].includes(placement)) {
    return triggerNodeSize?.left;
  }
  if (['topRight', 'bottomRight'].includes(placement)) {
    if (typeof tooltipNodeSize?.width !== 'number') return;
    return (
      triggerNodeSize?.left + (triggerNodeSize?.width - tooltipNodeSize?.width)
    );
  }
  if (isRight(placement)) {
    return triggerNodeSize?.left + triggerNodeSize?.width;
  }
  if (isLeft(placement)) {
    if (typeof tooltipNodeSize?.width !== 'number') return;
    return triggerNodeSize?.left - tooltipNodeSize?.width;
  }
  return triggerNodeSize?.left;
};

export const computedTop = (
  triggerNodeSize: Size | undefined,
  tooltipNodeSize: Size | undefined,
  placement: Placement,
) => {
  if (typeof triggerNodeSize?.top !== 'number') return;
  if (isTop(placement)) {
    if (typeof tooltipNodeSize?.height !== 'number') return;
    if (tooltipNodeSize?.height === 0) return -1000;
    return triggerNodeSize?.top - tooltipNodeSize?.height;
  }
  if (isBottom(placement)) {
    return triggerNodeSize?.top + triggerNodeSize?.height;
  }
  if (isRight(placement)) {
    if (typeof tooltipNodeSize?.top !== 'number') return;
    return (
      triggerNodeSize?.top -
      tooltipNodeSize?.height / 2 +
      triggerNodeSize?.height / 2
    );
  }
  if (isLeft(placement)) {
    if (typeof tooltipNodeSize?.top !== 'number') return;
    if (typeof tooltipNodeSize?.height !== 'number') return;
    if (tooltipNodeSize?.height === 0) return -1000;
    return (
      triggerNodeSize?.top -
      tooltipNodeSize?.height / 2 +
      triggerNodeSize?.height / 2
    );
  }
};
