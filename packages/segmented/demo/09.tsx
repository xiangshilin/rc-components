import { Appstore, Bars } from '@mumu-design/icons';
import { Segmented } from 'mumu-design';
import React from 'react';

export default () => (
  <Segmented
    options={[
      {
        value: 'List',
        icon: <Bars />,
      },
      {
        value: 'Kanban',
        icon: <Appstore />,
      },
    ]}
  />
);
