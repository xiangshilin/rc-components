import { Space } from 'mumu-design';
import React from 'react';

const App: React.FC = () => (
  <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
    <div>
      <p>Card content</p>
      <p>Card content</p>
    </div>
    <div>
      <p>Card content</p>
      <p>Card content</p>
    </div>
    <div>
      <p>Card content</p>
      <p>Card content</p>
    </div>
  </Space>
);

export default App;
