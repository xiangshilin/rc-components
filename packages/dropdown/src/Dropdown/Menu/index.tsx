import { classnames } from '@mumu-design/utils';
import React, {
  CSSProperties,
  PropsWithChildren,
  ReactNode,
  type FC,
} from 'react';
import './index.less';
import Option, { ItemType, OptionItemProps } from './Item';

export interface DropdownSelectProps extends PropsWithChildren {
  defaultOpenKeys?: string[];
  defaultSelectedKeys?: string[];
  expandIcon?:
    | ReactNode
    | ((props: OptionItemProps & { isSubMenu: boolean }) => ReactNode);
  forceSubMenuRender?: boolean;
  inlineCollapsed?: boolean;
  inlineIndent?: number;
  items?: ItemType[];
  mode?: 'vertical' | 'horizontal' | 'inline';
  multiple?: boolean;
  openKeys?: string[];
  overflowedIndicator?: ReactNode;
  selectable?: boolean;
  selectedKeys?: string[];
  style?: CSSProperties;
  subMenuCloseDelay?: number;
  subMenuOpenDelay?: number;
  theme?: 'light' | 'dark';
  triggerSubMenuAction?: 'hover' | 'click';
  onClick?: ({ item, key, keyPath, domEvent }: any) => void;
  onDeselect?: ({ item, key, keyPath, selectedKeys, domEvent }: any) => void;
  onOpenChange?: (openKeys: string[]) => void;
  onSelect?: ({ item, key, keyPath, selectedKeys, domEvent }: any) => void;
}

export const renderItem = (
  items: ItemType[] | undefined,
  children: React.ReactNode | undefined,
  onClick?: ({ item, key, keyPath, domEvent }: any) => void,
) => {
  if (Array.isArray(items)) {
    return items.map((item: any, index: number) => {
      return (
        <Option
          onTitleClick={({ item, key, domEvent }) => {
            onClick?.({ item, key, keyPath: '', selectedKeys: [], domEvent });
          }}
          key={item.key || index}
          id={item.key || index}
          {...item}
        />
      );
    });
  }
  return children;
};

const DropDownSelect: FC<DropdownSelectProps> = ({ items, onClick }) => {
  return (
    <div className={classnames('m-dropdown-menu')}>
      {renderItem(items, undefined, onClick)}
    </div>
  );
};

export default DropDownSelect;
