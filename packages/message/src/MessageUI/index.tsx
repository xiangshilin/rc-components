import { classnames } from '@mumu-design/utils';
import React, {
  CSSProperties,
  FunctionComponent,
  useEffect,
  useRef,
} from 'react';
import { ConfigProps } from '..';
import './index.less';
import MessageItem, { MessageItemProps } from './Item';

export interface MessageUIProps {
  list: MessageItemProps[];
  className?: string;
  style?: CSSProperties;
  onClose?: (key?: string) => void;
  onClick?: (key?: string) => void;
  defaultConfig?: ConfigProps;
}

const MessageUI: FunctionComponent<MessageUIProps> = ({
  className,
  style,
  onClick,
  onClose,
  list,
  defaultConfig,
}) => {
  const msgRef = useRef<HTMLDivElement>(null);

  /**
   * 记录元素起始位置
   */
  const record = () => {
    if (!msgRef.current?.children) return;
    for (let index = 0; index < msgRef.current?.children?.length; index++) {
      const dom = msgRef.current?.children?.[index] as any;
      const rect = dom.getBoundingClientRect();
      dom.startX = rect.left;
      dom.startY = rect.top;
    }
  };

  /**
   * 根据当前位置和起始位置差移动元素
   */
  const move = () => {
    if (!msgRef.current?.children) return;
    for (let index = 0; index < msgRef.current?.children?.length; index++) {
      const dom = msgRef.current?.children?.[index];
      const rect = dom.getBoundingClientRect();
      const curX = rect.left;
      const curY = rect.top;
      const startX = (dom as any).startX;
      const startY = (dom as any).startY;

      dom.animate(
        [
          { transform: `translate(${startX - curX}px, ${startY - curY}px)` },
          { transform: `translate(0px, 0px)` },
        ],
        {
          duration: 200,
        },
      );
    }
  };

  useEffect(() => {
    record();
  }, [JSON.stringify(list)]);

  return (
    <div
      ref={msgRef}
      className={classnames('m-message', className, {
        [`${defaultConfig?.prefixCls}-m-message`]: !!defaultConfig?.prefixCls,
      })}
      style={{
        top: defaultConfig?.top ? defaultConfig.top : undefined,
        ...style,
      }}
    >
      {list?.map((item) => {
        item.msgRef = item.msgRef?.current ? item.msgRef : { current: null };
        return (
          <MessageItem
            key={item.msgId}
            ref={item.msgRef}
            content={item.content}
            duration={
              typeof item.duration === 'number'
                ? item.duration
                : defaultConfig?.duration
            }
            icon={item.icon}
            type={item.type}
            record={record}
            style={item.style}
            className={item.className}
            move={move}
            onClick={() => {
              item.onClick?.();
              onClick?.(item.msgId);
            }}
            onClose={() => {
              item.onClose?.();
              onClose?.(item.msgId);
            }}
          />
        );
      })}
    </div>
  );
};

export default MessageUI;
