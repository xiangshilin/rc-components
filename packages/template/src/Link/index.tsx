import React, { FC, MouseEventHandler, PropsWithChildren } from 'react';
import './index.less';

export interface LinkProps extends PropsWithChildren {
  onClick?: MouseEventHandler<HTMLAnchorElement>;
}

const Link: FC<LinkProps> = ({ children, onClick }) => {
  return <a onClick={onClick}>{children}</a>;
};

export default Link;
