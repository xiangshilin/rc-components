import { Download } from '@mumu-design/icons';
import { Button, Divider, Radio, Space } from 'mumu-design';
import { SizeType } from 'mumu-design/button/src';
import React, { useState } from 'react';

const App: React.FC = () => {
  const [size, setSize] = useState<SizeType>('large'); // default is 'middle'

  return (
    <>
      <Radio.Group
        value={size}
        onChange={(v) => {
          setSize(v as SizeType);
        }}
      >
        <Radio.Button value="large">Large</Radio.Button>
        <Radio.Button value="default">Default</Radio.Button>
        <Radio.Button value="small">Small</Radio.Button>
      </Radio.Group>
      <Divider orientation="left" plain>
        Preview
      </Divider>
      <Space direction="vertical">
        <Space wrap>
          <Button type="primary" size={size}>
            Primary
          </Button>
          <Button size={size}>Default</Button>
          <Button type="dashed" size={size}>
            Dashed
          </Button>
        </Space>
        <Button type="link" size={size}>
          Link
        </Button>
        <Space wrap>
          <Button type="primary" icon={<Download />} size={size} />
          <Button
            type="primary"
            shape="circle"
            icon={<Download />}
            size={size}
          />
          <Button
            type="primary"
            shape="round"
            icon={<Download />}
            size={size}
          />
          <Button type="primary" shape="round" icon={<Download />} size={size}>
            Download
          </Button>
          <Button type="primary" icon={<Download />} size={size}>
            Download
          </Button>
        </Space>
      </Space>
    </>
  );
};

export default App;
