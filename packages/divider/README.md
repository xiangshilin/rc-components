---
demo:
  cols: 2
toc: content
group:
  title: 布局
  order: 2
---

# Divider 分割线

区隔内容的分割线。

## 何时使用

对不同章节的文本段落进行分割。
对行内文字/链接进行分割，例如表格的操作列。

## 代码演示

<code 
  src="./demo/01.tsx" 
  description="默认为水平分割线，可在中间加入文字。">
水平分割线
</code>
<code 
  src="./demo/02.tsx" 
  description="分割线中带有文字，可以用 `orientation` 指定文字位置。">
带文字的分割线
</code>
<code 
  src="./demo/03.tsx" 
  description="使用 `plain` 可以设置为更轻量的分割文字样式。">
分割文字使用正文样式
</code>
<code 
  src="./demo/04.tsx" 
  description="使用 `type='vertical'` 设置为行内的垂直分割线。">
垂直分割线
</code>
