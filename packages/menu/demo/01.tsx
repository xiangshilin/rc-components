import { Appstore, Mail, Setting } from '@mumu-design/icons';
import { MenuProps } from '@mumu-design/menu/src/Menu';
import { Menu } from 'mumu-design';
import React from 'react';

const App: React.FC = () => {
  const items: MenuProps['items'] = [
    {
      label: 'Navigation One',
      key: 'mail',
      icon: <Mail />,
    },
    {
      label: 'Navigation Two',
      key: 'app',
      icon: <Appstore />,
      disabled: true,
    },
    {
      label: 'Navigation Three - Submenu',
      key: 'SubMenu',
      icon: <Setting />,
      children: [
        {
          type: 'group',
          label: 'Item 1',
          children: [
            {
              label: 'Option 1',
              key: 'setting:1',
            },
            {
              label: 'Option 2',
              key: 'setting:2',
            },
          ],
        },
        {
          type: 'group',
          label: 'Item 2',
          children: [
            {
              label: 'Option 3',
              key: 'setting:3',
            },
            {
              label: 'Option 4',
              key: 'setting:4',
            },
          ],
        },
      ],
    },
    {
      label: (
        <a href="https://ant.design" target="_blank" rel="noopener noreferrer">
          Navigation Four - Link
        </a>
      ),
      key: 'alipay',
    },
  ];

  return <Menu items={items} />;
};

export default App;
