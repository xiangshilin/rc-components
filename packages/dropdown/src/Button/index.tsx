import Button from '@mumu-design/button';
import { Ellipsis } from '@mumu-design/icons';
import { MenuProps } from '@mumu-design/menu/src/Menu';
import React, { PropsWithChildren, type FC } from 'react';
import Dropdown from '../Dropdown';

export interface DropdownButtonProps extends PropsWithChildren {
  menu: MenuProps;
}

const DropdownButton: FC<DropdownButtonProps> = ({ children, menu }) => {
  return (
    <Button.Group>
      <Button>{children}</Button>
      <Dropdown menu={menu} trigger="click">
        <Button icon={<Ellipsis />}></Button>
      </Dropdown>
    </Button.Group>
  );
};

export default DropdownButton;
