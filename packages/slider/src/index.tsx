import React, { PropsWithChildren, type FC } from 'react';
import './index.less';

interface SliderProps extends PropsWithChildren {
  value?: number;
  onChange?: (v: number) => void;
}

const Slider: FC<SliderProps> = ({ children, value, onChange }) => {
  return (
    <label className="m-slider">
      <input
        type="range"
        value={value}
        onChange={(e) => {
          const val = e.target.value;
          onChange?.(+val);
        }}
      />
      {children}
    </label>
  );
};

export default Slider;
