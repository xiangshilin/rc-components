---
demo:
  cols: 2
toc: content
group:
  title: 反馈
  order: 1
---

# Message 全局提示

全局展示操作反馈信息

## 何时使用

- 可提示成功、警告和错误等反馈信息
- 顶部剧中显示并自动消失，是一种不打断用户操作的轻量级提示方式。

## 代码演示

<code 
  src="./demo/01.tsx" 
  description="信息提醒反馈。">
普通提示
</code>
<code 
  src="./demo/02.tsx" 
  description="包括成功、失败、警告。">
其他提示类型
</code>
<code 
  src="./demo/03.tsx" 
  description="自定义时长 10s，默认时长为 3s。">
修改延时
</code>
<code 
  src="./demo/04.tsx" 
  description="进行全局 loading，异步自行移除。">
加载中
</code>
<code 
  src="./demo/05.tsx" 
  description="可以通过 then 接口在关闭后运行 callback 。以上用例将在每个 message 将要结束时通过 then 显示新的 message 。">
Promise 接口
</code>
<code 
  src="./demo/06.tsx" 
  description="可以通过唯一的 key 来更新内容。">
更新消息内容
</code>
<code 
  src="./demo/07.tsx" 
  description="使用 style 和 className 来定义样式。">
自定义样式
</code>

<!-- <code
  src="./demo/08.tsx"
  description="通过 message.useMessage 创建支持读取 context 的 contextHolder。">
通过 Hooks 获取上下文
</code> -->
