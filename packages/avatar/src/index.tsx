import { classnames } from '@mumu-design/utils';
import React, {
  CSSProperties,
  FC,
  PropsWithChildren,
  ReactNode,
  useState,
} from 'react';
import './index.less';

interface AvatarProps extends PropsWithChildren {
  alt?: string;
  src?: string;
  style?: CSSProperties;
  icon?: ReactNode;
}

const Avatar: FC<AvatarProps> = ({ children, alt, src, style, icon }) => {
  const [imgLoadError, setImgLoadError] = useState(false);

  const render = () => {
    if (src) {
      if (imgLoadError) return alt;
      return (
        <img
          src={alt}
          onError={() => {
            setImgLoadError(true);
          }}
        />
      );
    }
    if (icon) {
      return icon;
    }
    if (children) return children;
  };

  return (
    <div
      style={style}
      className={classnames('m-avatar', {
        icon: !!icon,
      })}
    >
      {render()}
    </div>
  );
};

export default Avatar;
