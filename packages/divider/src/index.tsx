import { classnames } from '@mumu-design/utils';
import React, { PropsWithChildren, type FC } from 'react';
import './index.less';

interface DividerProps extends PropsWithChildren {
  /**
   * @description 分割线标题的位置
   * @default center
   */
  orientation?: 'left' | 'right' | 'center';
  /**
   * @description 文字是否显示为普通正文样式
   * @default false
   */
  plain?: boolean;
  type?: 'horizontal' | 'vertical';
  dashed?: boolean;
  orientationMargin?: string | number;
}

const Divider: FC<DividerProps> = ({
  children,
  orientation,
  plain,
  type,
  dashed,
}) => {
  if (type === 'vertical') {
    return <div className="m-divider m-divider-vertical"></div>;
  }
  return (
    <div
      style={{
        borderBlockStartStyle: dashed ? 'dashed' : 'solid',
      }}
      className={classnames('m-divider', {
        ['m-divider-with-text']: children,
        ['m-divider-with-text-left']: orientation === 'left',
        ['m-divider-with-text-right']: orientation === 'right',
        ['m-divider-plain']: plain,
      })}
    >
      <span className="m-divider-inner-text">{children}</span>
    </div>
  );
};

export default Divider;
