import { Button, message } from 'mumu-design';
import React from 'react';

const success = () => {
  message.success(
    'This is a prompt message for success, and it will disappear in 10 seconds',
    10,
  );
};

const App: React.FC = () => (
  <Button onClick={success}>Customized display duration</Button>
);

export default App;
