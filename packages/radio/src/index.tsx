import Group, { GroupProps } from './Group';
import Radio, { RadioProps } from './Radio';
import RadioButton, { RadioButtonProps } from './RadioButton';

const RadioExport = Radio as React.FC<RadioProps> & {
  Button: React.FC<RadioButtonProps>;
  Group: React.FC<GroupProps>;
};

RadioExport.Button = RadioButton;
RadioExport.Group = Group;

export default RadioExport;
