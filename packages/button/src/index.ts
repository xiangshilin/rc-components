import Button, { ButtonProps } from './Button';
import Group, { GroupProps } from './Group';

const ButtonExport = Button as typeof Button & {
  Group: React.FC<GroupProps>;
};

ButtonExport.Group = Group;

export default ButtonExport;

export type { ButtonProps };
