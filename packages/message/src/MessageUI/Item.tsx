import {
  CheckCircle,
  CloseCircle,
  Icon,
  InfoCircle,
  Loading,
} from '@mumu-design/icons';
import { classnames } from '@mumu-design/utils';
import React, {
  CSSProperties,
  forwardRef,
  ForwardRefRenderFunction,
  ReactElement,
  ReactNode,
  RefObject,
  useEffect,
  useImperativeHandle,
  useRef,
} from 'react';

export interface MessageItemProps {
  className?: string;
  /**
   * @description 提示内容
   */
  content: ReactElement | string;
  /**
   * @description 自动关闭的延时，单位秒。设为 0 时不自动关闭
   * @default 3
   */
  duration?: number;
  /**
   * @description 自定义图标
   */
  icon?: ReactNode;
  style?: CSSProperties;
  /**
   * @description 点击 message 时触发的回调函数
   */
  onClick?: () => void;
  /**
   * @description 关闭时触发的回调函数
   */
  onClose?: (key?: string) => void;
  type?: 'success' | 'error' | 'warning' | 'loading' | 'info';
  msgId?: string;
  key?: string;
  msgRef?: RefObject<MsgRef>;
}
type MsgRef = { close: () => Promise<any> };

const MessageItem: ForwardRefRenderFunction<
  MsgRef,
  MessageItemProps & {
    record: () => void;
    move: () => void;
  }
> = (
  {
    type,
    content,
    duration = 3,
    onClick,
    onClose,
    icon,
    record,
    move,
    style,
    className,
  },
  ref,
) => {
  const timer = useRef<NodeJS.Timeout>();
  const itemRef = useRef<HTMLDivElement>(null);

  const hideDom = () => {
    return new Promise((resolve) => {
      itemRef.current
        ?.animate(
          [
            { transform: `translateY(0)`, opacity: 1 },
            { transform: `translateY(-16px)`, opacity: 0 },
          ],
          {
            duration: 200,
          },
        )
        .finished.then(() => {
          resolve(null);
        });
    });
  };

  const clearTimer = () => {
    clearTimeout(timer.current);
    timer.current = undefined;
  };

  const close = () => {
    return new Promise((resolve) => {
      hideDom().then(() => {
        record?.();
        itemRef?.current?.remove?.();
        move?.();
        onClose?.();
        clearTimer();
        resolve(itemRef);
      });
    });
  };

  const getIcon = () => {
    if (icon) {
      return icon;
    }
    if (type === 'info') {
      return <InfoCircle style={{ color: '#4096ff' }} />;
    }
    if (type === 'success') {
      return <CheckCircle style={{ color: 'green' }} />;
    }
    if (type === 'warning') {
      return <InfoCircle style={{ color: 'orange' }} />;
    }
    if (type === 'error') {
      return <CloseCircle style={{ color: '#ff0000' }} />;
    }
    if (type === 'loading') {
      return <Icon spin component={<Loading style={{ color: '#4096ff' }} />} />;
    }
  };

  useEffect(() => {
    if (duration > 0) {
      timer.current = setTimeout(() => {
        close();
      }, duration * 1000);
    }
    return () => {
      clearTimer();
    };
  }, []);

  useImperativeHandle(ref, () => {
    return {
      close,
    };
  });

  return (
    <div style={style} className={classnames('m-message-wrap', className)}>
      <div
        // 不能直接移除根节点，故ref挂载在子元素上
        // 因为react DOM DIff是对比不同的节点，如果移除根节点会导致在mouted阶段没有根节点，故需增加一个可标识的节点
        ref={itemRef}
        onClick={onClick}
        className={classnames('m-message-wrap-content')}
      >
        <span className="m-message-wrap-content-icon">{getIcon()}</span>
        <span>{content}</span>
      </div>
    </div>
  );
};

const MessageItemRef = forwardRef(MessageItem);

export default MessageItemRef;
