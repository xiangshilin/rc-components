import { classnames } from '@mumu-design/utils';
import React, {
  Children,
  CSSProperties,
  PropsWithChildren,
  ReactNode,
  type FC,
} from 'react';
import './index.less';

export type SpaceSize = 'small' | 'middle' | 'large' | number;

export interface SpaceProps extends PropsWithChildren {
  /**
   * @description 对齐方式
   */
  align?: 'start' | 'end' | 'center' | 'baseline';
  /**
   * @description 间距方向
   * @default "horizontal"
   */
  direction?: 'vertical' | 'horizontal';
  /**
   * @description 间距大小
   * @default "small"
   */
  size?: SpaceSize | SpaceSize[];
  /**
   * @description 设置拆分
   */
  split?: ReactNode;
  /**
   * @description 是否自动换行，仅在 horizontal 时有效
   * @default false
   */
  wrap?: boolean;
  style?: CSSProperties;
  className?: string;
}

const SIZE_MAP = {
  small: 8,
  middle: 16,
  large: 24,
};

const Space: FC<SpaceProps> = ({
  children,
  size = 'small',
  style,
  direction = 'horizontal',
  align,
  wrap,
  split,
  className,
}) => {
  const getGap = () => {
    if (size === 'small') {
      return SIZE_MAP.small;
    }
    if (size === 'middle') {
      return SIZE_MAP.middle;
    }
    if (size === 'large') {
      return SIZE_MAP.large;
    }
    if (typeof size === 'number') {
      return size;
    }
    return SIZE_MAP.small;
  };

  return (
    <div
      className={classnames(className, 'm-space', {
        ['m-space-vertical']: direction === 'vertical',
        ['m-space-align-center']:
          direction === 'horizontal' || align === 'center',
        ['m-space-align-start']: align === 'start',
        ['m-space-align-end']: align === 'end',
        ['m-space-align-baseline']: align === 'baseline',
      })}
      style={{
        gap: getGap(),
        flexWrap: wrap ? 'wrap' : 'nowrap',
        ...style,
      }}
    >
      {Children.toArray(children).map((Node, index) => {
        return (
          <React.Fragment key={index}>
            <div className="m-space-item">{Node}</div>
            {index !== Children.toArray(children).length - 1 && split}
          </React.Fragment>
        );
      })}
    </div>
  );
};

export default Space;
