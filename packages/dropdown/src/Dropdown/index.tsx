import Tooltip, { TooltipProps } from '@mumu-design/tooltip';
import { classnames } from '@mumu-design/utils';
import React, { Children, cloneElement, type FC } from 'react';
import './index.less';
import Select, { DropdownSelectProps } from './Menu';

export interface DropdownProps extends TooltipProps {
  /**
   * @description 菜单配置项
   */
  menu: DropdownSelectProps;
  className?: string;
}

const Dropdown: FC<DropdownProps> = ({
  children,
  menu,
  className,
  trigger = 'hover',
  placement = 'bottomLeft',
  arrow = false,
  ...rest
}) => {
  return (
    <Tooltip
      color="#fff"
      trigger={trigger}
      title={<Select items={menu.items} />}
      placement={placement}
      arrow={arrow}
      overlayInnerStyle={{
        padding: 0,
        color: '#333',
      }}
    >
      {Children.toArray(children).map((Node: any) => {
        return cloneElement(Node, {
          ...Node.props,
          ...rest,
          className: classnames('m-dropdown', className),
        });
      })}
    </Tooltip>
  );
};

export default Dropdown;
