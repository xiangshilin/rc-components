---
demo:
  cols: 2
toc: content
group:
  title: 数据录入
  order: 3
---

# AutoComplete 自动完成

输入框自动完成功能。

## 何时使用

- 需要一个输入框而不是选择器。
- 需要输入建议/辅助提示。

和 Select 的区别是：

- AutoComplete 是一个带提示的文本输入框，用户可以自由输入，关键词是辅助输入。
- Select 是在限定的可选项中进行选择，关键词是选择。

## 代码演示

<code 
  src="./demo/01.tsx" 
  description="基本使用，通过 options 设置自动完成的数据源。">
基本使用
</code>
<code 
  src="./demo/02.tsx" 
  description="可以返回自定义的 Option label">
自定义选项
</code>
