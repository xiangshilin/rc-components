import { Segmented } from 'mumu-design';
import React from 'react';

export default () => (
  <Segmented
    block
    options={[123, 456, 'longtext-longtext-longtext-longtext']}
  />
);
