import Button, { ButtonProps } from '@mumu-design/button';
import { classnames } from '@mumu-design/utils';
import React, { type CSSProperties, type FC } from 'react';
import './index.less';

export interface RadioButtonProps extends ButtonProps {
  onChange?: (val: string | number | readonly string[] | undefined) => void;
  name?: string;
  value?: string | number | readonly string[] | undefined;
  style?: CSSProperties;
  defaultValue?: string | number | readonly string[] | undefined;
}

const RadioButton: FC<RadioButtonProps> = ({
  children,
  onChange,
  value,
  defaultValue,
  className,
  ...btnProps
}) => {
  return (
    <Button
      {...btnProps}
      className={classnames(
        'm-radio-group-btn-item',
        {
          ['m-selected']: defaultValue === value,
        },
        className,
      )}
      onClick={() => {
        onChange?.(value);
      }}
    >
      {children}
    </Button>
  );
};

export default RadioButton;
