---
demo:
  cols: 1
toc: content
---

# Segmented 分段控制器

分段控制器。

## 何时使用

用于展示多个选项并允许用户选择其中单个选项；
当切换选中选项时，关联区域的内容会发生变化。

## 代码演示

<code
  src="./demo/01.tsx"
  description="最简单的用法。">
基本
</code>

<code
  src="./demo/02.tsx"
  description="`block` 属性使其适合父元素宽度。">
Block 分段选择器
</code>

<code
  src="./demo/03.tsx"
  description="Segmented 不可用。">
不可用
</code>

<code
  src="./demo/04.tsx"
  description="受控的 Segmented。">
受控模式
</code>

<code
  src="./demo/05.tsx"
  description="使用 ReactNode 自定义渲染每一个 Segmented Item。">
自定义渲染
</code>

<code
  src="./demo/06.tsx"
  description="动态加载数据。">
动态数据
</code>

<code
  src="./demo/07.tsx"
  description="我们为 `Segmented` 组件定义了三种尺寸（大、默认、小），高度分别为 40px、32px 和 24px。">
三种大小
</code>

<code
  src="./demo/08.tsx"
  description="给 Segmented Item 设置 Icon。">
设置图标
</code>

<code
  src="./demo/09.tsx"
  description="在 Segmented Item 选项中只设置 Icon。">
只设置图标
</code>
