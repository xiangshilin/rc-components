## Space

|   参数    |                 说明                 |              类型               |   默认值   |
| :-------: | :----------------------------------: | :-----------------------------: | :--------: |
|   align   |               对齐方式               | start / end / center / baseline |     -      |
| direction |               间距方向               |      vertical / horizontal      | horizontal |
|   size    |               间距大小               |          Size / Size[]          |   small    |
|   split   |               设置拆分               |            ReactNode            |     -      |
|   wrap    | 是否自动换行，仅在 horizontal 时有效 |             boolean             |   false    |
