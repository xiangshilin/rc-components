import { classnames } from '@mumu-design/utils';
import React, {
  Children,
  cloneElement,
  CSSProperties,
  PropsWithChildren,
  type FC,
} from 'react';
import './index.less';

export interface CompactProps extends PropsWithChildren {
  className?: string;
  style?: CSSProperties;
}

const Compact: FC<CompactProps> = ({ children, className }) => {
  return (
    <div className={classnames(className, 'm-space-compact', {})}>
      {Children.toArray(children).map((Node: any, index) => {
        return cloneElement(Node, {
          ...Node.props,
          key: index,
          className: 'm-space-compact-item',
        });
      })}
    </div>
  );
};

export default Compact;
