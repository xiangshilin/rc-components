---
hero:
  title: component
  description:
  actions:
    - text: 立即上手
      link: /guide
    - text: 代码仓库
      link: https://gitee.com/xiangshilin/rc-components
features:
  - title: UI
    emoji: 🌈
    description: 提炼自企业级中后台产品的交互语言和视觉风格。
  - title: 更好的编译性能
    emoji: 📦
    description: 开箱即用的高质量 `React` 组件。
  - title: TypeScript
    emoji: 🛡
    description: 使用 TypeScript 开发，提供完整的类型定义文件。
---

## 反馈与共建

请访问 代码仓库 。
