import React, { FC, MouseEventHandler, PropsWithChildren } from 'react';

export interface TextProps extends PropsWithChildren {
  onClick?: MouseEventHandler<HTMLAnchorElement>;
}

const Text: FC<TextProps> = ({ children, onClick }) => {
  return <a onClick={onClick}>{children}</a>;
};

export default Text;
