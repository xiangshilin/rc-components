# Tooltip 文字提示

简单的文字提示气泡框。

## 何时使用

鼠标移入则显示提示，移出消失，气泡浮层不承载复杂文本和操作。

可用来代替系统默认的 title 提示，提供一个 按钮/文字/操作 的文案解释。

## 代码演示

<code
  src="./demo/01.tsx"
  description="最简单的用法。">
基本
</code>

<code
  src="./demo/02.tsx"
  description="位置有 12 个方向。">
位置
</code>

<code
  src="./demo/03.tsx"
  description="通过 arrow 属性隐藏箭头。">
箭头展示
</code>

<code
  src="./demo/04.tsx"
  description="我们添加了多种预设色彩的文字提示样式，用作不同场景使用。">
多彩文字提示
</code>
