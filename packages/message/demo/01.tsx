import { Button, message } from 'mumu-design';
import React from 'react';

const info = () => {
  message.info('This is a normal message');
};

const App: React.FC = () => (
  <Button type="primary" onClick={info}>
    Display normal message
  </Button>
);

export default App;
