import Link, { LinkProps } from './Link';
import Paragraph, { ParagraphProps } from './Paragraph';
import Text, { TextProps } from './Text';
import Title, { TitleProps } from './Title';
import Typography, { TypographyProps } from './Typography';

const TypographyExport = Typography as React.FC<TypographyProps> & {
  Link: React.FC<LinkProps>;
  Text: React.FC<TextProps>;
  Title: React.FC<TitleProps>;
  Paragraph: React.FC<ParagraphProps>;
};

TypographyExport.Link = Link;
TypographyExport.Text = Text;
TypographyExport.Title = Title;
TypographyExport.Paragraph = Paragraph;

export default TypographyExport;
