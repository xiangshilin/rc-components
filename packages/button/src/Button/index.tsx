import { classnames } from '@mumu-design/utils';
import React, {
  forwardRef,
  ForwardRefRenderFunction,
  PropsWithChildren,
  ReactNode,
  useRef,
} from 'react';
import './index.less';
import WaterWave from './WaterWave';

export type SizeType = 'large' | 'middle' | 'small';

export interface ButtonProps extends PropsWithChildren {
  /**
   * @description 将按钮宽度调整为其父宽度的选项
   * @default false
   */
  block?: boolean;
  /**
   * @description 设置危险按钮
   * @default false
   */
  danger?: boolean;
  /**
   * @description 设置按钮失效状态
   * @default false
   */
  disabled?: boolean;
  /**
   * @description 幽灵属性，使按钮背景透明
   * @default false
   */
  ghost?: boolean;
  /**
   * @description 点击跳转的地址，指定此属性 button 的行为和 a 链接一致
   */
  href?: string;
  /**
   * @description 设置 button 原生的 type 值，可选值请参考 HTML 标准
   * @default "button"
   */
  htmlType?: string;
  /**
   * @description 设置按钮的图标组件
   */
  icon?: ReactNode;
  /**
   * @description 设置按钮载入状态
   * @default false
   */
  loading?: boolean | { delay: number };
  /**
   * @description 设置按钮形状
   * @default 'default'
   */
  shape?: 'default' | 'circle' | 'round';
  /**
   * @description 设置按钮大小
   * @default 'middle'
   */
  size?: SizeType;
  /**
   * @description 相当于 a 链接的 target 属性，href 存在时生效
   */
  target?: string;
  /**
   * @description 设置按钮类型
   * @default 'default'
   */
  type?: 'primary' | 'ghost' | 'dashed' | 'link' | 'text' | 'default';
  /**
   * @description 点击按钮时的回调
   */
  onClick?: React.MouseEventHandler<HTMLButtonElement>;
  className?: string;
  groupIdx?: number;
  groupLength?: number;
}

const Button: ForwardRefRenderFunction<any, ButtonProps> = (
  {
    children,
    type = 'default',
    onClick,
    icon,
    shape,
    href,
    size = 'middle',
    className,
    disabled,
    ghost,
    loading,
    danger,
    block,
    groupIdx,
    groupLength,
    ...rest
  },
  ref,
) => {
  const waterWaveRef = useRef<any>(null);

  const getIcon = () => {
    if (loading) {
      return <div className="m-btn-loading-icon"></div>;
    }

    return icon && <span>{icon}</span>;
  };

  const isIconOnly = (loading || icon) && !children;

  const getTextMarginLeft = () => {
    if ((loading || icon) && children) return 8;
    return 0;
  };

  return (
    <button
      ref={ref}
      type="button"
      disabled={disabled}
      className={classnames(
        'm-btn',
        `m-btn-${type}`,
        {
          ['m-btn-block']: block,
          ['m-btn-lg']: size === 'large',
          ['m-btn-sm']: size === 'small',
          ['m-btn-circle']: shape === 'circle',
          ['m-btn-loading']: loading,
          ['m-btn-icon-only']: isIconOnly,
          ['m-btn-lg-icon-only']: isIconOnly && size === 'large',
          ['m-btn-sm-icon-only']: isIconOnly && size === 'small',
          [`dangerous`]: danger,
          [`background-ghost`]: ghost,
        },
        className,
      )}
      onClick={(e) => {
        if (!loading) {
          waterWaveRef.current?.add();
        }
        if (href) return (window.location.href = href);
        onClick?.(e);
      }}
      {...rest}
    >
      {getIcon()}
      <span style={{ marginLeft: getTextMarginLeft() }}>{children}</span>
      <WaterWave
        ref={waterWaveRef}
        shape={shape}
        type={type}
        groupIdx={groupIdx}
        groupLength={groupLength}
      />
    </button>
  );
};

export default forwardRef(Button);
