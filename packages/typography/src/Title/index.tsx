import React, { FC, MouseEventHandler } from 'react';
import Base from '../Base';
import { TextProps } from '../Text';

export interface TitleProps extends TextProps {
  onClick?: MouseEventHandler<HTMLAnchorElement>;
  level?: 1 | 2 | 3 | 4 | 5;
}

const Title: FC<TitleProps> = ({ children, level = 1 }) => {
  const render = () => {
    if (level === 1) return <h1>{children}</h1>;
    if (level === 2) return <h2>{children}</h2>;
    if (level === 3) return <h3>{children}</h3>;
    if (level === 4) return <h4>{children}</h4>;
    if (level === 5) return <h5>{children}</h5>;
  };

  return <Base>{render()}</Base>;
};

export default Title;
