import React, {
  type ChangeEventHandler,
  type CSSProperties,
  type FC,
  type PropsWithChildren,
} from 'react';

export interface RadioProps extends PropsWithChildren {
  onChange?: ChangeEventHandler<HTMLInputElement> | undefined;
  name?: string;
  value?: string | number | readonly string[] | undefined;
  style?: CSSProperties;
  defaultChecked?: boolean;
}

const Radio: FC<RadioProps> = ({
  children,
  onChange,
  name,
  value,
  style,
  defaultChecked,
}) => {
  return (
    <label>
      <input
        defaultChecked={defaultChecked}
        style={style}
        name={name}
        value={value}
        onChange={onChange}
        type="radio"
      />
      {children}
    </label>
  );
};

export default Radio;
