import { defineConfig } from 'father';

export default defineConfig({
  // more father config: https://github.com/umijs/father/blob/master/docs/config.md
  esm: {
    transformer: 'babel',
    output: 'dist/es',
    input: 'packages',
    ignores: ['**/dist/**/*.*', '**/node_modules/**/*.*', '**/demo/**/*.*'],
  },
  umd: {
    output: 'dist/umd',
    entry: 'packages',
  },
});
