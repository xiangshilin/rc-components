import Space from '@mumu-design/space';
import React, {
  Children,
  cloneElement,
  PropsWithChildren,
  type FC,
} from 'react';

export type GroupProps = PropsWithChildren;

const Group: FC<GroupProps> = ({ children }) => {
  const _children = Children.toArray(children);
  return (
    <Space.Compact>
      {_children.map((Node: any, index: number) => {
        return cloneElement(Node, {
          ...Node.props,
          groupIdx: index,
          groupLength: _children.length,
        });
      })}
    </Space.Compact>
  );
};

export default Group;
