import { Appstore, Bars } from '@mumu-design/icons';
import { Segmented } from 'mumu-design';
import React from 'react';

export default () => (
  <Segmented
    options={[
      {
        label: 'List',
        value: 'List',
        icon: <Bars />,
      },
      {
        label: 'Kanban',
        value: 'Kanban',
        icon: <Appstore />,
      },
    ]}
  />
);
