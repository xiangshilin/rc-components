/**
 * inline: true
 */
import * as Icon from '@mumu-design/icons';
import { Space } from 'mumu-design';
import React, { createElement } from 'react';

const App: React.FC = () => (
  <Space wrap>
    {Object.keys(Icon).map((key) => {
      if (!(Icon as any)[key]) return <></>;
      return createElement((Icon as any)[key], {
        style: {
          fontSize: 36,
        },
      });
    })}
  </Space>
);

export default App;
