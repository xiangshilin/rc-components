import Tooltip from '@mumu-design/tooltip';
import React, {
  cloneElement,
  CSSProperties,
  FC,
  PropsWithChildren,
  ReactElement,
  useEffect,
  useState,
} from 'react';
import './index.less';
import Menu from './Menu';

export interface AutoCompleteProps extends PropsWithChildren {
  options?: { label?: string; value: string }[];
  style?: CSSProperties;
  onSelect?: (data: string) => void;
  onSearch?: (searchText: string) => void;
  placeholder?: string;
  value?: string;
  onChange?: (data: string) => void;
}

const AutoComplete: FC<AutoCompleteProps> = ({
  options,
  style,
  onSelect,
  onSearch,
  onChange,
  placeholder,
  value,
  children,
}) => {
  const [open, setOpen] = useState(false);
  const [selfValue, setSelfValue] = useState('');

  useEffect(() => {
    onChange?.(selfValue);
  }, [selfValue]);

  return (
    <Tooltip
      color="#fff"
      overlayInnerStyle={{
        padding: 0,
        color: '#333',
        width: 200,
      }}
      arrow={false}
      open={open}
      placement={'bottomLeft'}
      title={
        <Menu
          options={options?.map((item) => ({
            label: item?.label || item?.value,
          }))}
          onClick={({ item }) => {
            onSelect?.(item?.label);
            setSelfValue(item?.label);
            setOpen(false);
          }}
        />
      }
    >
      <div style={style} className="m-auto-complete">
        {cloneElement(children as ReactElement, {
          ...(children as any).props,
          onChange: (e: { target: { value: any } }) => {
            const val = e.target.value;
            onSearch?.(val);
            setSelfValue?.(val);
            setOpen(!!val);
          },
          onFocus: () => {
            setOpen(!!value || !!selfValue);
          },
          onBlur: () => {
            setOpen(false);
          },
          placeholder,
          type: 'text',
          value: value || selfValue,
        })}
      </div>
    </Tooltip>
  );
};

export default AutoComplete;
