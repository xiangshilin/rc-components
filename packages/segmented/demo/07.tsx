import { Segmented, Space } from 'mumu-design';
import React from 'react';

const App: React.FC = () => (
  <Space direction="vertical">
    <Segmented
      size="large"
      options={['Daily', 'Weekly', 'Monthly', 'Quarterly', 'Yearly']}
    />
    <Segmented
      options={['Daily', 'Weekly', 'Monthly', 'Quarterly', 'Yearly']}
    />
    <Segmented
      size="small"
      options={['Daily', 'Weekly', 'Monthly', 'Quarterly', 'Yearly']}
    />
  </Space>
);

export default App;
