import { Search } from '@mumu-design/icons';
import { Button, Space, Tooltip } from 'mumu-design';
import React from 'react';

const App: React.FC = () => (
  <Space direction="vertical">
    <Space wrap>
      <Tooltip title="search">
        <Button type="primary" shape="circle" icon={<Search />} />
      </Tooltip>
      <Button type="primary" shape="circle">
        A
      </Button>
      <Button type="primary" icon={<Search />}>
        Search
      </Button>
      <Tooltip title="search">
        <Button shape="circle" icon={<Search />} />
      </Tooltip>
      <Button icon={<Search />}>Search</Button>
    </Space>
    <Space wrap>
      <Tooltip title="search">
        <Button shape="circle" icon={<Search />} />
      </Tooltip>
      <Button icon={<Search />}>Search</Button>
      <Tooltip title="search">
        <Button type="dashed" shape="circle" icon={<Search />} />
      </Tooltip>
      <Button type="dashed" icon={<Search />}>
        Search
      </Button>
      <Button icon={<Search />} href="https://www.google.com" />
    </Space>
  </Space>
);

export default App;
